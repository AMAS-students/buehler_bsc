\documentclass[xcolor=pdftex,table,10pt,yellow,mathserif]{beamer}
\usepackage[numbers]{natbib}
\usepackage{array}
\usepackage{amsmath}
\usepackage{ulem}
\usepackage{algorithm2e}
\usepackage{tikz}
\usepackage{pgflibraryshapes}
\usepackage{beamerthemesplit}
\mode<presentation> {
\usetheme{PSI}
}
\usepackage[english]{babel}
\usetikzlibrary{shapes,arrows,snakes,backgrounds,arrows.meta}
\usepackage[overlay,absolute]{textpos}
\usepackage[utf8]{inputenc}
\usepackage{pgfpages}
\usepackage{ulem}
\usetikzlibrary{mindmap,trees}
\usepackage{verbatim}
\usepackage{pgfpages}
\usepackage{bbding}
\xdefinecolor{mygreen}{RGB}{0,220,0}
\xdefinecolor{myblue}{RGB}{26,150,255}

\usepackage{datetime}
\usepackage{pgf-umlcd}
\usepackage{pgf-umlsd}
\usepackage{pgfgantt}
\usepackage{minted}

\newcommand{\R}[0]{\mathbb{R}}
\newcommand{\C}[0]{\mathbb{C}}
\newcommand{\limtoinf}[1]{\lim_{#1 \to \infty}}
%\newcommand{\norm}[2]{\left\lVert#1\right\rVert_{#2}}
\newcommand{\partderiv}[2]{\frac{\partial #2}{\partial #1}}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\mat}[1]{\mathbf{#1}}



\newcommand{\qvec}{\mathbf{q}}
\newcommand{\lieoperator}[1]{{:\!#1\!:\!}}
\newcommand{\poissonbracket}[1]{ \left[ #1 \right] }
\newcommand{\lietransformation}[1]{e^{:\!#1\!:}}

\newcommand*\tick{\item[\color{mygreen}\Checkmark]}

\bibliographystyle{IEEEtran}



\newtheorem{thm}{Theorem}

% ************************ space ************************************
\newcommand{\hhs}[1]{\hspace{#1mm}}
\newcommand{\hs}{\hspace{5mm}}
\newcommand{\vp}{\vspace{1mm}}
\newcommand{\vs}{\vspace{5mm}}
\newcommand{\jl}{$\frac{}{}$} %User defined for empty symbol to jump line
% ********************** newcommand *********************************
\newcommand{\mbf}[1]{\mbox{\boldmath $#1$}}
\newcommand{\hb}[1]{\hspace{-#1 mm}}
\newcommand{\ds}{\displaystyle}
\newcommand{\QED}{\hfill $\Box$}
% *********************** frequently used math symbols from AMS *******
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}

\newcounter{myWeekNum}
\stepcounter{myWeekNum}
\newcommand{\myWeek}{\themyWeekNum
    \stepcounter{myWeekNum}
    \ifnum\themyWeekNum=53
         \setcounter{myWeekNum}{1}
    \else\fi
}

\definecolor{Maroon}{rgb}{.5,.1,.25}% Used for Trinity University


\mode<presentation>
{
  \usepackage{pgf}
  \usepackage{hyperref}
  \setbeamercovered{invisible}
  %\setbeamercovered{transparent}  
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}


\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\setbeamercolor{uppercolgreen}{fg=white,bg=yellow!35}
\setbeamercolor{lowercolgreen}{fg=black,bg=yellow!90}


\title{Building Blocks for Finite Element computations in IPPL}

\subtitle{Bachelor Thesis}

\author{Lukas Bühler}

\newdate{presdate}{30}{1}{2024}
\date{\displaydate{presdate}}

\titlegraphic{
 \includegraphics[width=10cm]{./figures/figures-slides/PSI} \\
}

\begin{document}

\frame{
    \maketitle
}
%
\frame{
    \tableofcontents
}


\section{Introduction}

\begin{frame}{Introduction}
    Currently, IPPL supports electrostatic PIC simulations.\\

    Development of a full electromagnetic (EM) solver is ongoing.\\

    The Finite Element Method (FEM) is one of the numerical methods used in EM solvers.\\

    Goal: Implement the building blocks for the FEM in IPPL.
\end{frame}

\begin{frame}[t]{EM Solver Schemes}
    Common in EM solvers: Finite Difference Time Domain (FDTD) scheme. (2nd-order accuracy)
    \begin{itemize}
        \item Space discretization with Finite Differences.
        \item Time discretization with Finite Differences.
    \end{itemize}
    \vspace*{0.5cm}
    \pause

    Finite Element Time Domain (FETD)
    \begin{itemize}
        \item Space discretization using FEM.
        \item Time discretization with other schemes, Runge-Kutta methods.
    \end{itemize}
    \vspace*{0.5cm}
\end{frame}

\begin{frame}{Advantages of FEM vs. Finite Differences}

    Advantages of using FEM compared to Finite Differences
    \begin{itemize}
        \item More complex geometries
              \pause

        \item Higher-order elements: Using higher-order basis functions ($p$-refinement)

              Improve accuracy without affecting runtime, scalability and memory footprint.
              \pause

        \item Still possible to do mesh refinement ($h$-refinement).
              \pause

        \item ... or both ($hp$-refinement).
              \pause

        \item Even smaller memory footprint with a matrix-free assembly algorithm.

              $\Rightarrow$ Better performance on GPUs.
    \end{itemize}
\end{frame}

\begin{frame}[t]{Finite Element Method (FEM)}
    The Finite Element Method is used to solve PDEs.\\

    Steps:
    \begin{enumerate}
        \item Discretization (Meshing) of the domain with elements.
        \item Approximating the solution of the PDE on the elements.
        \item Assembling the approximated solutions of the elements in a LSE.
    \end{enumerate}

    \pause

    \begin{equation}
        \begin{aligned}
            \mat{A}\vec{\mu} = \vec{\varphi}
        \end{aligned}
    \end{equation}

    The LSE then needs to be solved.\\

    FEM produces sparse problems $\Rightarrow$ Use a sparse solver.\\

    \pause

    We use the Conjugate Gradient (CG) method to iteratively solve the LSE.
    This allows us to use a matrix-free assembly method.
\end{frame}


\begin{frame}{Motivation for Matrix-free Method}
    Ljungkvist, 2017: Matrix-free finite element algorithms have many benefits on modern manycore processors and GPUs compared to sparse matrix-vector products. \cite{ljungkvist_matrix-free_2017}\\

    Settgast et.\ al, 2023: The matrix-free approach, in the context of the CG method, compares favorably even for low-order FEM. \cite{settgast_performant_2023}
\end{frame}


\begin{frame}{Conjugate Gradient (CG) Method}
    The CG method is an iterative method to approximate the solution of a LSE.

    \begin{algorithm}[H]
        \scriptsize
        \vspace{0.3cm}
        $\vec{x} \gets \text{initial guess, }(\text{usually }\vec{0})$

        $\vec{b} \gets \vec{\varphi}$

        $\vec{p} \gets \mat{A} \vec{x} \quad$ \only<2>{\tcp{Stiffness matrix used here}}

        $\vec{r} \gets \vec{b} - \vec{p}$

        \While{$\|\vec{r}\|_2 < \epsilon$}{
            \vspace{0.1cm}
            $\vec{z} \gets \mat{A}\vec{p} \quad$ \only<2>{\tcp{Stiffness matrix used here}}

            \vspace{0.2cm}
            $\displaystyle \alpha \gets \frac{\vec{r}^\top \vec{r}}{\vec{p}^\top \vec{z}}$
            \vspace{0.2cm}

            $\vec{x} \gets \vec{x} + \alpha \vec{p}$

            $\vec{r}_\text{old} \gets \vec{r}$

            $\vec{r} \gets \vec{r} - \alpha \vec{z}$

            \vspace{0.2cm}
            $\displaystyle \beta \gets \frac{\vec{r}^\top \vec{r}}{\vec{r}_\text{old}^\top \vec{r}_\text{old}}$
            \vspace{0.2cm}

            $\vec{p} \gets \vec{r} + \beta \vec{p}$
        }
    \end{algorithm}
\end{frame}


% TODO add IPPL slide for audiences other than the IPPL crew


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Framework \& Implementation}


\begin{frame}[t]{\texttt{FEMPoissonSolver}}
    The \texttt{FEMPoissonSolver} is a proof of concept FEM solver.\\

    It solves the Poisson equation for a given right-hand side function.
    \begin{equation}
        \begin{aligned}
            -\Delta u & = f \  &  & u \in \Omega,         \\
            u         & = 0 \  &  & u \in \partial\Omega.
        \end{aligned}
        \label{eq:strong_form}
    \end{equation}

    Currently uses:
    \begin{itemize}
        \item Homogeneous Dirichlet boundary conditions
        \item IPPL uniform meshes
        \item 1st-order Lagrangian finite elements
        \item Gauss-Jacobi quadrature
    \end{itemize}
\end{frame}


\begin{frame}[t]{Solver and Assembly}
    What \texttt{FEMPoissonSolver} does:
    \begin{enumerate}
        \item Precompute the transformations, the quadrature weights and quadrature nodes
        \item Define the ``eval'' lambda function (for the Poisson equation)
        \item Use the CG solver with the matrix-free assembly function (\texttt{evaluateAx}) to solve the problem
    \end{enumerate}

    \only<2>{
        \[
            \vec{z} = \mat{A}\vec{x}
        \]}
    \only<3->{
        \[
            \vec{z} = \texttt{evaluateAx}(\texttt{eval}, \vec{x})
        \]}

    \only<4->{
        For the Poisson equation:
        \[
            \texttt{eval}(i, j, k) := (\mat{D \Phi}_K(\hat{\vec{q}}_k))^{-\top}
            \nabla \hat{b}^j(\hat{\vec{q}}_k)
            \cdot
            (\mat{D \Phi}(\hat{\vec{q}}_k))^{-\top}
            \nabla \hat{b}^i(\hat{\vec{q}}_k) \,
            |\det \mat{D \Phi}_K(\hat{\vec{q}}_k)|
        \]}
\end{frame}

\begin{frame}{Assembly Prerequisites}
    Strong form:
    \begin{equation}
        \begin{aligned}
            -\Delta u & = f \  &  & u \in \Omega,         \\
            u         & = 0 \  &  & u \in \partial\Omega.
        \end{aligned}
        \tag{\ref{eq:strong_form}}
    \end{equation}
    \pause

    Weak form (variational formulation):
    \begin{equation}
        \begin{aligned}
            \only<2>{\int_\Omega \nabla u \cdot \nabla v \ d\vec{x}
                = \int_\Omega f \, v \ d\vec{x}.}
            \only<3->{\underbrace{\int_\Omega \nabla u \cdot \nabla v \ d\vec{x}}_{=: \, \mathrm{a}(u,v)}
                = \underbrace{\int_\Omega f \, v \ d\vec{x}}_{=: \, \ell(v)}.}
        \end{aligned}
    \end{equation}
    \pause
    \pause

    (Element) stiffness matrix and load vector:
    \begin{align}
        \mat{A}   & = \left[\mathrm{a}(b_h^J, b_h^I)\right]_{I,J=1}^N, \  & \vec{\varphi} = \left[\ell(b_h^I)\right]_{I=1}^N,   \\
        \mat{A}_K & = \left[\mathrm{a}(b_K^j, b_K^i)\right]_{i,j=1}^M, \  & \vec{\varphi}_K = \left[\ell(b_K^i)\right]_{i=1}^M,
    \end{align}

    \scriptsize

    with  $b_K^i := b_{h|K}^i$,
    for element $K$,

    $N$ number of global basis functions,
    $M$ number of local shape functions.
\end{frame}


\begin{frame}[t]{Matrix-free Assembly Derivation (\texttt{evaluateAx})}

    \begin{columns}[T]
        \begin{column}{.85\textwidth}
            \begin{algorithm}[H]
                \scriptsize%\small
                \linespread{1.35}\selectfont
                \KwIn{$\vec{x}$}

                $\vec{z} \gets \vec{0}$

                \For{Element $K$ in Mesh}{
                \tcp{\color{darkgray}1. Compute the Element matrix $\mathbf{A}_K$}

                $\text{DOFs}_K \gets \{ 4, 5, 8, 7 \}$,
                $\text{DOFs}_{\hat{K}} \gets \{ 0, 1, 2, 3 \}$

                \only<1>{\dots}
                \only<2->{
                \For{$i, j \in \text{DOFs}_{\hat{K}}$}{
                \only<2-3>{
                $I =\text{DOFs}_{K}[i]$,
                $J = \text{DOFs}_K[j]$
                }

                % Global
                \only<2>{$\displaystyle
                        (\mathbf{A}_K)_{i,j} = \int_{K} \nabla b_K^J(\vec{x}) \cdot \nabla b_K^I(\vec{x}) \ d\vec{x}
                    $}

                % Local integration domain
                \only<3>{$\displaystyle
                        (\mathbf{A}_K)_{i,j} = \int_{\hat{K}}
                        \mathbf{\Phi}^*_K
                        \nabla b_K^J \cdot
                        \mathbf{\Phi}^*_K \nabla b_K^I
                        |\det \mathbf{D \Phi}_K |
                        \ d\hat{\vec{x}}
                    $}

                % Fully local
                \only<4>{$\displaystyle
                        (\mathbf{A}_K)_{i,j} = \int_{\hat{K}}
                        (\mathbf{D \Phi}_K)^{-\top}
                        \nabla \hat{b}^j \cdot
                        (\mathbf{D \Phi}_K)^{-\top}
                        \nabla \hat{b}^i
                        |\det \mathbf{D \Phi}_K|
                        \ d\hat{\vec{x}}
                    $}

                % Quadrature
                \only<5->{
                    \setlength{\abovedisplayskip}{0pt}
                    \setlength{\belowdisplayskip}{0pt}
                    \setlength{\abovedisplayshortskip}{0pt}
                    \setlength{\belowdisplayshortskip}{0pt}
                    \begin{align*}
                        (\mathbf{A}_K)_{i,j} \approx \sum_{k}^{N_\text{Int}} &
                        \hat{\omega}_k
                        (\mathbf{D \Phi}_K(\hat{\vec{q}}_k))^{-\top}
                        \nabla \hat{b}^j(\hat{\vec{q}}_k)                            \\
                                                                             & \cdot
                        (\mathbf{D \Phi}(\hat{\vec{q}}_k))^{-\top}
                        \nabla \hat{b}^i(\hat{\vec{q}}_k)
                        |\det \mathbf{D \Phi}_K(\hat{\vec{q}}_k)| \qquad \qquad \qquad
                    \end{align*}}
                }
                }

                \tcp{\color{darkgray}2. Compute $\vec{z} = \mathbf{A}\vec{x}$ contribution with $\mathbf{A}_K$}
                \only<1-5>{\dots}
                \only<6->{
                \For{$i, j \in \text{DOFs}_{\hat{K}}$} {
                $I =\text{DOFs}_{K}[i]$,
                $J = \text{DOFs}_K[j]$

                $\vec{z}_I \gets \vec{z}_I + (\mathbf{A}_K)_{i,j} \cdot \vec{x}_J$
                }}
                }
                \KwRet{$\vec{z}$}
            \end{algorithm}

        \end{column}
        \begin{column}{.2\textwidth}
            \begin{figure}
                \centering
                \begin{tikzpicture}

                    \only<3-6>{\fill[red, opacity=0.1] (0,3) rectangle ++(1.5,1.5);}

                    % Reference quad
                    \draw[step=1.5cm,gray,very thin] (0,3) grid (1.5,4.5);
                    \node[red, font=\tiny, anchor=south west] at (0,3) {0};
                    \fill[red] (0,3) circle (2pt);
                    \node[red, font=\tiny, anchor=south east] at (1.5,3) {1};
                    \fill[red] (1.5,3) circle (2pt);
                    \node[red, font=\tiny, anchor=north east] at (1.5,4.5) {2};
                    \fill[red] (1.5,4.5) circle (2pt);
                    \node[red, font=\tiny, anchor=north west] at (0,4.5) {3};
                    \fill[red] (0,4.5) circle (2pt);
                    \node[red, font=\scriptsize] at (.75,3.75) {$\hat{K}$};

                    \only<2-3>{\fill[red, opacity=0.1] (1,1) rectangle ++(1,1);}

                    % Draw grid over highlighted cells
                    \draw[step=1cm,gray,very thin] (0,0) grid (2,2);
                    \foreach \x in {0,1,2} {
                            \foreach \y in {0,1,2} {
                                    \fill (\x,\y) circle (2pt);
                                    \pgfmathtruncatemacro\index{3*\y + \x}
                                    \node[black, font=\tiny, anchor=south west] at (\x,\y) {\index};
                                }
                        }

                    \node[black, font=\scriptsize] at (1.5,1.5) {$K$};

                \end{tikzpicture}

            \end{figure}
        \end{column}
    \end{columns}

\end{frame}


\begin{frame}{Matrix-free Assembly Algorithm (Poisson equation)}
    \begin{algorithm}[H]
        \tiny
        \linespread{1.35}\selectfont
        \KwIn{$\vec{x}$, $\texttt{eval}(i, j, k)$}

        $\vec{z} \gets \vec{0}$ {\color{darkgray}\tcp{Resulting vector to return}}

        \For{Element $K$ in Mesh}{

        $\texttt{localDOFs} \gets \texttt{getLocalDOFsForElement}(K)$

        $\texttt{globalDOFs} \gets \texttt{getGlobalDOFsForElement}(K)$

        {\color{darkgray}\tcp{1. Compute the Element matrix $\mat{A}_K$}}

        \For{$i \in \texttt{localDOFs}$}{
        \For{$j \in \texttt{localDOFs}$}{
        $\displaystyle
            (\mat{A}_K)_{i,j} \approx \sum_{k}^{N_\text{Int}}
            \hat{\omega}_k
            \underbrace{
                (\mat{D \Phi}_K(\hat{\vec{q}}_k))^{-\top}
                \nabla \hat{b}^j(\hat{\vec{q}}_k)
                \cdot
                (\mat{D \Phi}(\hat{\vec{q}}_k))^{-\top}
                \nabla \hat{b}^i(\hat{\vec{q}}_k) \,
                |\det \mat{D \Phi}_K(\hat{\vec{q}}_k)|
            }_{=:\texttt{eval}(i, j, k)}
        $
        }}

        {\color{darkgray}\tcp{2. Compute $\vec{z} = \mat{A}\vec{x}$ contribution with $\mat{A}_K$}}
        \For{$i\in \texttt{localDOFs}$} {
            $I = \texttt{globalDOFs}[i]$

            \For{$j \in \texttt{localDOFs}$} {
                $J = \texttt{globalDOFs}[j]$

                $\vec{z}_I \gets \vec{z}_I + (\mat{A}_K)_{i,j} \cdot \vec{x}_J$
            }
        }
        }
        \KwRet{$\vec{z}$}
        \vspace{0.5cm}
    \end{algorithm}
\end{frame}


\begin{frame}{Software Architecture}
    \begin{figure}[h]
        \centering
        \resizebox{\hsize}{!}{
            \begin{tikzpicture}

                \begin{class}[text width=5cm]{FiniteElementSpace}{0,0}
                    \attribute{\{abstract\}}
                    \attribute{typename T}
                    \attribute{unsigned Dim}
                    \attribute{unsigned NumElementDOFs}
                    \attribute{typename QuadratureType}
                    \attribute{typename FieldLHS}
                    \attribute{typename FieldRHS}
                \end{class}

                \begin{class}[text width=5cm]{LagrangeSpace}{6.25,0}
                    \attribute{typename T}
                    \attribute{unsigned Dim}
                    \attribute{unsigned Order}
                    \attribute{typename QuadratureType}
                    \attribute{typename FieldLHS}
                    \attribute{typename FieldRHS}
                \end{class}
                \draw[-{Triangle[length=2mm,width=2mm,fill=white]}, umlcolor, line width=0.2mm] (LagrangeSpace) -- (FiniteElementSpace);

                \begin{class}[text width=3cm]{Mesh}{-5.5, 4}
                    \attribute{\{abstract\}}
                    \attribute{typename T}
                    \attribute{unsigned Dim}
                \end{class}
                \aggregation{FiniteElementSpace}{}{}{Mesh}

                % ELEMENTS

                \begin{class}[text width=4cm]{Element}{0, 4}
                    \attribute{\{abstract\}}
                    \attribute{typename T}
                    \attribute{unsigned Dim}
                    \attribute{unsigned NumVertices}
                \end{class}
                \aggregation{FiniteElementSpace}{}{}{Element}

                \begin{class}[text width=4cm]{EdgeElement}{-4.5, 6.5}
                    \attribute{typename T}
                \end{class}
                \draw[-{Triangle[length=2mm,width=2mm,fill=white]}, umlcolor, line width=0.2mm] (EdgeElement) -- (Element);

                \begin{class}[text width=4cm]{QuadrilateralElement}{0, 6.5}
                    \attribute{typename T}
                \end{class}
                \draw[-{Triangle[length=2mm,width=2mm,fill=white]}, umlcolor, line width=0.2mm] (QuadrilateralElement) -- (Element);

                \begin{class}[text width=4cm]{HexahedralElement}{4.5, 6.5}
                    \attribute{typename T}
                \end{class}
                \draw[-{Triangle[length=2mm,width=2mm,fill=white]}, umlcolor, line width=0.2mm] (HexahedralElement) -- (Element);

                % QUADRATURE

                \begin{class}[text width=4cm]{Quadrature}{6, 4}
                    \attribute{\{abstract\}}
                    \attribute{typename T}
                    \attribute{unsigned NumNodes1D}
                    \attribute{typename ElementType}
                \end{class}
                \aggregation{FiniteElementSpace}{}{}{Quadrature}

                \begin{class}[text width=5cm]{GaussJacobiQuadrature}{12, 3.5}
                    \attribute{typename T}
                    \attribute{unsigned NumNodes1D}
                    \attribute{typename ElementType}
                \end{class}
                \draw[-{Triangle[length=2mm,width=2mm,fill=white]}, umlcolor, line width=0.2mm] (GaussJacobiQuadrature) -- (Quadrature);

                \begin{class}[text width=5cm]{MidpointQuadrature}{12, 6.5}
                    \attribute{typename T}
                    \attribute{unsigned NumNodes1D}
                    \attribute{typename ElementType}
                \end{class}
                \draw[-{Triangle[length=2mm,width=2mm,fill=white]}, umlcolor, line width=0.2mm] (MidpointQuadrature) -- (Quadrature);

                % SOLVER

                \begin{class}[text width=4cm]{FEMPoissonSolver}{12,-1}
                    \attribute{typename FieldLHS}
                    \attribute{typename FieldLHS}
                \end{class}
                \aggregation{FEMPoissonSolver}{}{}{LagrangeSpace}
                \aggregation{FEMPoissonSolver}{}{}{GaussJacobiQuadrature}

            \end{tikzpicture}
        }
        \caption{Software architecture of the FEM framework, showing the classes with their template arguments.}
        \label{fig:software_arch_uml}
    \end{figure}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Results \& Conclusion}

\begin{frame}[t]{Sinusoidal Problem}
    Problem:
    \begin{equation}
        \begin{aligned}
            -\Delta u & = \pi^2 \sin(\pi x), \  &  & x \in [-1, 1],   \\
            u(x)      & = 0, \                  &  & x \in \{-1, 1\}.
        \end{aligned}
    \end{equation}
    \vspace*{0.5cm}

    Exact solution:
    \begin{align}
        u(x) = \sin(\pi x).
    \end{align}
    \vspace*{0.5cm}

    Mesh spacing: $h = \frac{2}{n - 1}.$
\end{frame}

\begin{frame}[t]{Results}
    \begin{center}
        \only<1>{\includegraphics[height=4cm]{figures/fem_convergence_sinus1d.png}}
        \only<2>{\includegraphics[height=5cm]{figures/fem_convergence_sinus2d.png}}
        \only<3>{\includegraphics[height=5cm]{figures/fem_convergence_sinus3d.png}}
    \end{center}

    \only<1>{
        Expected convergence is order 2.\\

        The trapezoidal quadrature rule converges
        rapidly when applied to analytic functions on periodic intervals.
        \cite{trefethen_exponentially_2014}
    }
\end{frame}

\begin{frame}{Conclusion}
    Goal: Implement the building blocks for FEM with the possibility for $p$-refinement.\\

    \pause

    Achieved:\pause
    \begin{itemize}
        \item Software Architecture Design\pause
        \item Building blocks implemented\pause
              \begin{itemize}
                  \item Mesh-Element-Mapping\pause
                  \item Element classes with dimension-independent affine transformations\pause
                  \item 1st-order Lagrangian finite elements (dimension independent, extensible to higher-order)\pause
                  \item Quadrature base class and tensor-product quadrature\pause
                  \item Gauss-Jacobi Quadrature with an iterative algorithm to compute the roots of the Jacobi-Polynomial\pause
                  \item Matrix-free Assembly Algorithm, with local evaluations, interfacing with the CG algorithm
              \end{itemize}
              \pause
        \item Proof of concept solver implemented (\texttt{FEMPoissonSolver})
    \end{itemize}
    \pause

    $\quad \Rightarrow$ A working beginning of a FEM framework in IPPL

    \vspace*{0.5cm}

\end{frame}

\begin{frame}[t]{Future Work}
    \begin{itemize}
        \item Higher-order Lagrangian finite elements ($p$-refinement)

              \pause

        \item Adding support for more boundary conditions
              \begin{itemize}
                  \item non-homogeneous Dirichlet boundary conditions
                  \item (periodic boundary conditions)
              \end{itemize}

              \pause

        \item Parallelization, GPU support, scaling studies

              \pause

        \item Adding more elements and finite element spaces
              \begin{itemize}
                  \item Nédélec
                  \item Raviart-Thomas
              \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}[t]{Bibliography}
    \scriptsize
    \bibliography{thesis.bib}
\end{frame}

\end{document}